package api

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/strava-likes-telebot/models"

	"golang.org/x/net/html"

	"bitbucket.org/strava-likes-telebot/dom"
)

// Activity is repr of Strava acitvity
type Activity struct {
	ID        int
	Rank      string
	Title     string
	Athlete   string
	UpdatedAt time.Time
}

type context struct {
	Token string
}

const (
	stravaURL          string = "https://www.strava.com"
	stravaDashboardURL string = "https://www.strava.com/dashboard"
	userAgent          string = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36 OPR/56.0.3051.104"
)

func getPageToken(n *html.Node) interface{} {
	token, _ := dom.IterateElement(n, func(n *html.Node) (interface{}, bool) {
		var token string
		var isToken = false
		if n.Type == html.ElementNode && n.Data == "meta" {
			for _, a := range n.Attr {
				if a.Key == "name" && a.Val == "csrf-token" {
					isToken = true
				}
			}
			for _, a := range n.Attr {
				if a.Key == "content" && isToken {
					token = a.Val
				}
			}
		}
		if isToken {
			return token, true
		}
		return "", false
	})

	return token
}

func getStravaDashboardPage(token string) (string, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", stravaDashboardURL, nil)
	if err != nil {
		return "", err
	}
	req.Header.Add("cookie", token)
	req.Header.Add("user-agent", userAgent)
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	b, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close()
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func isActivityBlock(n *html.Node) bool {
	if n.Type == html.ElementNode && n.Data == "div" {
		for _, a := range n.Attr {
			if a.Key == "class" {
				return strings.Contains(a.Val, "activity") && strings.Contains(a.Val, "feed-entry")
			}
		}
	}
	return false
}

func getTitle(n *html.Node) string {
	elm := dom.FindElementByTagName(n, "h3")
	if elm == nil {
		return ""
	}
	return dom.GetTextContent(elm)
}

func getAthlete(n *html.Node) string {
	elm := dom.FindElementByClassName(n, "entry-owner")
	if elm == nil {
		return ""
	}
	return dom.GetTextContent(elm)
}

func getActivity(n *html.Node) Activity {
	var act Activity

	if n.Type == html.ElementNode && n.Data == "div" {
		for _, a := range n.Attr {
			switch a.Key {
			case "data-rank":
				act.Rank = a.Val
			case "data-updated-at":
				parsedTime, err := strconv.ParseInt(a.Val, 10, 64)
				var updatedAt time.Time
				if err != nil {
					updatedAt = time.Now().UTC()
				} else {
					updatedAt = time.Unix(parsedTime, 0)
				}
				act.UpdatedAt = updatedAt
			case "id":
				if id, err := strconv.Atoi(strings.TrimPrefix(a.Val, "Activity-")); err == nil {
					act.ID = id
				} else {
					act.ID = -1
				}
			}
		}
	}

	act.Title = getTitle(n)
	act.Athlete = getAthlete(n)

	return act
}

// SendKudos send kudo
func SendKudos(activityID int, user *models.User) error {
	body, err := getStravaDashboardPage(user.Token)
	if err != nil {
		return err
	}
	q, _ := html.Parse(strings.NewReader(body))
	token := getPageToken(q).(string)

	client := &http.Client{}
	kudoURL := fmt.Sprintf("https://www.strava.com/feed/activity/%d/kudo", activityID)
	req, err := http.NewRequest("POST", kudoURL, strings.NewReader(`{"id":1}`))
	req.Header.Add("cookie", user.Token)
	req.Header.Add("origin", stravaURL)
	req.Header.Add("referer", stravaDashboardURL)
	req.Header.Add("user-agent", userAgent)
	req.Header.Add("x-csrf-token", token)
	req.Header.Add("x-requested-with", "XMLHttpRequest")

	_, err = client.Do(req)
	if err != nil {
		return err
	}

	return nil
}

// GetLatestActivities returns strava activities afer specified date
func GetLatestActivities(user *models.User) ([]Activity, error) {
	var acitvities []Activity

	body, err := getStravaDashboardPage(user.Token)
	if err != nil {
		return nil, err
	}
	q, _ := html.Parse(strings.NewReader(body))
	var f func(*html.Node)
	f = func(n *html.Node) {
		if isActivityBlock(n) {
			acitvities = append(acitvities, getActivity(n))
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(q)

	return acitvities, nil
}
