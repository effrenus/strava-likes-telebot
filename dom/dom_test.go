package dom

import (
	"reflect"
	"testing"

	"golang.org/x/net/html"
)

func TestIterateElement(t *testing.T) {
	type args struct {
		n         *html.Node
		extractFN extractFN
	}
	tests := []struct {
		name  string
		args  args
		want  interface{}
		want1 bool
	}{
	// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := IterateElement(tt.args.n, tt.args.extractFN)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("IterateElement() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("IterateElement() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestFindElementByTagName(t *testing.T) {
	type args struct {
		n   *html.Node
		tag string
	}
	tests := []struct {
		name string
		args args
		want *html.Node
	}{
	// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FindElementByTagName(tt.args.n, tt.args.tag); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindElementByTagName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetTextContent(t *testing.T) {
	type args struct {
		n *html.Node
	}
	tests := []struct {
		name string
		args args
		want string
	}{
	// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetTextContent(tt.args.n); got != tt.want {
				t.Errorf("GetTextContent() = %v, want %v", got, tt.want)
			}
		})
	}
}
