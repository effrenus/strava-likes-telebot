package dom

import (
	"strings"

	"golang.org/x/net/html"
)

type extractFN func(n *html.Node) (interface{}, bool)

// IterateElement probe each value with `extractFN`
func IterateElement(n *html.Node, extractFN extractFN) (interface{}, bool) {
	if val, found := extractFN(n); found {
		return val, true
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		if val, found := IterateElement(c, extractFN); found {
			return val, true
		}
		continue
	}
	return nil, false
}

// FindElementByTagName find node with specific tag name
func FindElementByTagName(n *html.Node, tag string) *html.Node {
	var elm *html.Node
	if n.Type == html.ElementNode && n.Data == tag {
		return n
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		elm = FindElementByTagName(c, tag)
		if elm == nil {
			continue
		}
		return elm
	}
	return nil
}

func FindElementByClassName(n *html.Node, className string) *html.Node {
	var elm *html.Node
	if n.Type == html.ElementNode {
		isFound := false
		for _, a := range n.Attr {
			if a.Key != "class" {
				continue
			}
			classNames := strings.Split(className, " ")
			for i, name := range classNames {
				if !strings.Contains(a.Val, name) {
					break
				} else if i == len(classNames)-1 {
					isFound = true
				}
			}
		}
		if isFound {
			return n
		}
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		elm = FindElementByClassName(c, className)
		if elm == nil {
			continue
		}
		return elm
	}
	return nil
}

// GetTextContent returns nodes text content
func GetTextContent(n *html.Node) string {
	text := ""
	if n.Type == html.TextNode {
		return strings.Trim(n.Data, " \n")
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		text += GetTextContent(c)
	}
	return text
}
