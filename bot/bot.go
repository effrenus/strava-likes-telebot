package bot

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
	tb "gopkg.in/tucnak/telebot.v2"

	"bitbucket.org/strava-likes-telebot/api"
	"bitbucket.org/strava-likes-telebot/models"
)

var (
	bot *tb.Bot
)

func handleStart(m *tb.Message, user *models.User) {
	if len(user.Token) == 0 {
		bot.Send(m.Sender, "Please, send Strava token in format `token: YOUR_TOKEN` Otherwise the bot will not fetch friends activities.")
		return
	}
	bot.Send(m.Sender, "Welcome back!")
}

func contains(activities []models.Activity, activityID int) bool {
	for _, a := range activities {
		if a.ActivityID == activityID {
			return true
		}
	}
	return false
}

func handleDigest(m *tb.Message, user *models.User) {
	activities, err := api.GetLatestActivities(user)
	if err != nil {
		log.Panic(err)
	}

	processedActivities := models.GetProcessedActivities(user)

	for _, activity := range activities {
		if contains(processedActivities, activity.ID) {
			continue
		}

		(&models.Activity{
			ActivityID: activity.ID,
			UserRefer:  user.ID,
		}).Save()

		inlineBtn := tb.InlineButton{
			Unique: fmt.Sprintf("activity_%d", activity.ID),
			Text:   "Give Kudos!",
			Data:   strconv.Itoa(activity.ID),
		}
		inlineKeys := [][]tb.InlineButton{
			[]tb.InlineButton{inlineBtn},
		}
		bot.Send(m.Sender, fmt.Sprintf("*%s*\n%s", activity.Athlete, activity.Title), &tb.ReplyMarkup{
			InlineKeyboard: inlineKeys,
		}, tb.ModeMarkdown)

	}

}

func onCallback(c *tb.Callback) {
	user := models.GetUserByID(c.Sender.ID)
	acitivtyID, err := strconv.Atoi(strings.Split(c.Data, "|")[1])
	if err != nil {
		bot.Respond(c, &tb.CallbackResponse{
			CallbackID: c.ID,
			Text:       "Invalid activity ID",
		})
		return
	}
	api.SendKudos(acitivtyID, user)
	bot.Edit(c.Message, fmt.Sprintf("*👍* %s", c.Message.Text), tb.ModeMarkdown)
	bot.Respond(c, &tb.CallbackResponse{
		CallbackID: c.ID,
		Text:       "Success",
	})
}

// Start intialize telegrm bot
func Start(db *gorm.DB) {
	botToken := os.Getenv("BOT_TOKEN")
	if len(botToken) == 0 {
		log.Panicln("Empty token")
	}

	var err error
	bot, err = tb.NewBot(tb.Settings{
		Token:  botToken,
		Poller: &tb.LongPoller{Timeout: 5 * time.Second},
	})
	if err != nil {
		log.Panicln(err)
	}

	bot.Handle("/start", models.WithUser(db, handleStart))
	bot.Handle("/digest", models.WithUser(db, handleDigest))
	bot.Handle(tb.OnCallback, onCallback)
	bot.Handle(tb.OnText, models.WithUser(db, func(m *tb.Message, user *models.User) {
		if !strings.HasPrefix(m.Text, "token:") {
			bot.Send(m.Sender, "Send token in format: `token: YOUR_TOKEN`")
			return
		}
		user.Token = strings.TrimPrefix(m.Text, "token:")
		user.Save()
		bot.Send(m.Sender, "Token saved. Thank you.")
	}))

	bot.Start()
}
