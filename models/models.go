package models

import (
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	tb "gopkg.in/tucnak/telebot.v2"
)

var db *gorm.DB

type User struct {
	ID         int `gorm:"primary_key"`
	FirstName  string
	LastName   string
	Token      string
	Activities []Activity `gorm:"foreignkey:UserRefer"`
}

func (user *User) Save() {
	db.Save(user)
}

type Activity struct {
	gorm.Model
	ActivityID int
	UserRefer  int
}

func (activity *Activity) Save() {
	db.Create(activity)
}

// WithUser is wrapper to populate func with user
func WithUser(db *gorm.DB, in func(*tb.Message, *User)) func(*tb.Message) {
	return func(m *tb.Message) {
		var user User
		sender := m.Sender

		db.Where(User{ID: sender.ID}).Find(&user)

		if user.ID == 0 {
			user = User{
				ID:        sender.ID,
				FirstName: sender.FirstName,
				LastName:  sender.LastName,
			}
			db.NewRecord(&user)
		}

		in(m, &user)
	}
}

// GetUserByID return user
func GetUserByID(id int) *User {
	var user User
	db.Where("ID = ?", id).First(&user)

	return &user
}

func GetProcessedActivities(user *User) []Activity {
	var activities []Activity
	db.Model(&Activity{}).Where("user_refer = ?", user.ID).Find(&activities)

	return activities
}

// InitDB open DB session and create User table if doesn't exists
func InitDB() (*gorm.DB, error) {
	var err error
	db, err = gorm.Open("postgres", os.Getenv("DB_PATH"))

	if !db.HasTable(&User{}) {
		db.CreateTable(&User{})
	}

	if !db.HasTable(&Activity{}) {
		db.CreateTable(&Activity{})
	}

	return db, err
}
