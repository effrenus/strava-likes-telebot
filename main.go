package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"

	"bitbucket.org/strava-likes-telebot/bot"
	"bitbucket.org/strava-likes-telebot/models"
)

func startServer() {
	port := os.Getenv("PORT")

	if port == "" {
		log.Fatal("$PORT must be set")
	}

	router := gin.New()

	router.GET("/", func(c *gin.Context) {
		c.Status(http.StatusForbidden)
	})

	router.GET("/healthz/", func(c *gin.Context) {
		c.String(http.StatusOK, "OK")
	})

	router.Run(":" + port)
}

func main() {
	db, err := models.InitDB()
	if err != nil {
		log.Fatalln("DB error: ", err)
	}
	defer db.Close()

	go startServer()
	bot.Start(db)

}
